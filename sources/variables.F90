!!******************************************************************************
!!
!!  This file is part of the GODUNOV source code, a program to perform
!!  Newtonian or relativistic magnetohydrodynamical simulations.
!!
!!  Copyright (C) 2007-2020 Grzegorz Kowal <grzegorz@amuncode.org>
!!
!!  This program is free software: you can redistribute it and/or modify
!!  it under the terms of the GNU General Public License as published by
!!  the Free Software Foundation, either version 3 of the License, or
!!  (at your option) any later version.
!!
!!  This program is distributed in the hope that it will be useful,
!!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!!  GNU General Public License for more details.
!!
!!  You should have received a copy of the GNU General Public License
!!  along with this program.  If not, see <http://www.gnu.org/licenses/>.
!!
!!******************************************************************************
!!
!! module: VARIABLES
!!
!!  This module handles variables, their allocations and deallocation.  It also
!!  provides indices to variables, both conservative and primitive, and their
!!  names. It contains some global variables as well.
!!
!!******************************************************************************
!
module variables

#ifdef PROFILE
! include external subroutines
!
  use timers, only : set_timer, start_timer, stop_timer
#endif /* PROFILE */

! module variables are not implicit by default
!
  implicit none

#ifdef PROFILE
! timer indices
!
  integer, save      :: ivi
#endif /* PROFILE */

! parameters for numbers of independent variables:
!
!   nf - the number of fluid variables
!   ne - the number of energy variables in the case of adiabatic equation of
!        state
!   nb - the number of magnetic variables
!   nj - the number of current density components + anomalous resistivity
!
  integer, parameter :: nf = 4
#ifdef ISO
  integer, parameter :: ne = 0
#endif /* ISO */
#ifdef ADI
  integer, parameter :: ne = 1
#endif /* ADI */
#ifdef BFIELD
#ifdef GLM
  integer, parameter :: nb = 4
#else /* GLM */
  integer, parameter :: nb = 3
#endif /* GLM */
#ifdef RESISTIVITY
#ifdef ANOMALOUS
  integer, parameter :: nj = 4
#else /* ANOMALOUS */
  integer, parameter :: nj = 3
#endif /* ANOMALOUS */
#else /* RESISTIVITY */
  integer, parameter :: nj = 0
#endif /* RESISTIVITY */
#else /* BFIELD */
  integer, parameter :: nb = 0
  integer, parameter :: nj = 0
#endif /* BFIELD */

! number of fluid variables nu, the number of evolved variables nt, and
! the total number of variables stored in array qp(:,:,:,:)
!
  integer, parameter :: nu = nf + ne
  integer, parameter :: nt = nu + nb
  integer, parameter :: nn = nt + nj

! the indices and number of variables for boundary exchange
!
#if defined HYDRO || defined GLM
  integer, parameter :: nk = nt
  integer, parameter :: nl = nt
#else /* HYDRO | GLM */
  integer, parameter :: nk = nt + 1
  integer, parameter :: nl = nt + nb
#endif /* HYDRO | GLM */

! directional indices
!
  integer, parameter :: inx = 1, iny = 2, inz = 3

! variable indices
!
  integer, parameter :: idn = 1
  integer, parameter :: imx = 2, imy = 3, imz = 4
  integer, parameter :: ivx = 2, ivy = 3, ivz = 4
#ifdef ADI
  integer, parameter :: ien = 5, ipr = 5
#endif /* ADI */
#ifdef BFIELD
#ifdef GLM
  integer, parameter :: ibx = nu + 1, iby = nu + 2, ibz = nu + 3
  integer, parameter :: iph = nu + 4
#else /* GLM */
  integer, parameter :: iax = nu + 1, iay = nu + 2, iaz = nu + 3
  integer, parameter :: ibx = nu + 1, iby = nu + 2, ibz = nu + 3
  integer, parameter :: iex = nu + 1, iey = nu + 2, iez = nu + 3
#endif /* GLM */
#ifdef RESISTIVITY
  integer, parameter :: ijx = nt + 1, ijy = nt + 2, ijz = nt + 3
#ifdef ANOMALOUS
  integer, parameter :: iet = nt + 4
#endif /* ANOMALOUS */
#endif /* RESISTIVITY */
#endif /* BFIELD */

! arrays to store the names of conservative and primitive variables
!
  character(len=4), dimension(:), allocatable, save :: cvars, pvars

! allocatable arrays for cell averaged conserved variables and their increments
!
  real, dimension(:,:,:,:), allocatable, target :: u0, u1, du

! pointer to the current array
!
  real, dimension(:,:,:,:), pointer             :: uu

! allocatable array to store primitive variables
!
  real, dimension(:,:,:,:), allocatable         :: qp

#ifdef VPOT
! allocatable arrays for interface centered averages of the magnetic field
! components, used only in constrained transport schemes based on vector
! potential A
!
  real, dimension(:,:,:,:), allocatable         :: bi
#endif /* VPOT */

#ifdef SHOCK_DETECTION
! allocatable array to store the shock presence indicators
!
  logical, dimension(:,:,:), allocatable        :: sh
#endif /* SHOCK_DETECTION */
  logical, dimension(:)    , allocatable        :: sx, sy, sz

! allocatable arrays for one dimensional vectors of variables and fluxes
!
  real, dimension(:,:)    , allocatable         :: qx, qy, qz
  real, dimension(:,:)    , allocatable         :: fx, fy, fz

! boundary values of the primitive variables
!
  real, dimension(:,:,:), save, allocatable     :: qp_bnd

! uniform component of the magnetic field
!
  real, dimension(3), save                      :: buni  = 0.0d+00

! the maximum acceleration and speed in the system
!
  real, save                                    :: amax  = 0.0d+00
  real, save                                    :: cmax  = 0.0d+00
  real, save                                    :: cmax2 = 0.0d+00

! by default everything is public
!
  public

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
!
  contains
!
!===============================================================================
!
! subroutine INITIALIZE_VARIABLES:
! -------------------------------
!
!   Subroutine allocates memory for all variable arrays.
!
!   Arguments:
!
!     im, jm, km - domain dimensions along the X, Y, and Z directions;
!
!===============================================================================
!
  subroutine initialize_variables(im, jm, km)

! include external procedures
!
    use parameters, only : get_parameter

! local variables are not implicit by default
!
    implicit none

! dimensions of the variable arrays
!
    integer, intent(in) :: im, jm, km
!
!-------------------------------------------------------------------------------
!
#ifdef PROFILE
! set timer descriptions
!
    call set_timer('variable initialization', ivi)

! start accounting time for variable initialization
!
    call start_timer(ivi)
#endif /* PROFILE */

! allocate variables
!
    allocate(u0(nt,im,jm,km))
    allocate(u1(nt,im,jm,km))
    allocate(du(nt,im,jm,km))
    allocate(qp(nn,im,jm,km))
#ifdef VPOT
    allocate(bi(nb,im,jm,km))
#endif /* VPOT */
#ifdef SHOCK_DETECTION
    allocate(sh(   im,jm,km))
#endif /* SHOCK_DETECTION */
    allocate(sx(im))
    allocate(sy(jm))
    allocate(sz(km))

! allocate arrays for one dimensional vectors of variables and fluxes
!
    allocate(qx(nt,im))
    allocate(qy(nt,jm))
    allocate(qz(nt,km))
    allocate(fx(nt,im))
    allocate(fy(nt,jm))
    allocate(fz(nt,km))

! allocate arrays for variable names
!
    allocate(cvars(nt))
    allocate(pvars(nn))

! allocate arrays for the boundary values
!
    allocate(qp_bnd(nt,3,2))

! initialize the boundary values
!
    qp_bnd(:,:,:) = 0.0d+00

! initialize the uniform components of magnetic field
!
    call get_parameter("bunix", buni(1))
    call get_parameter("buniy", buni(2))
    call get_parameter("buniz", buni(3))

! read the user defined boundary values
!
    call get_parameter("dn_bnd_xl", qp_bnd(idn,1,1))
    call get_parameter("dn_bnd_xr", qp_bnd(idn,1,2))
    call get_parameter("dn_bnd_yl", qp_bnd(idn,2,1))
    call get_parameter("dn_bnd_yr", qp_bnd(idn,2,2))
    call get_parameter("dn_bnd_zl", qp_bnd(idn,3,1))
    call get_parameter("dn_bnd_zr", qp_bnd(idn,3,2))
    call get_parameter("vx_bnd_xl", qp_bnd(ivx,1,1))
    call get_parameter("vx_bnd_xr", qp_bnd(ivx,1,2))
    call get_parameter("vx_bnd_yl", qp_bnd(ivx,2,1))
    call get_parameter("vx_bnd_yr", qp_bnd(ivx,2,2))
    call get_parameter("vx_bnd_zl", qp_bnd(ivx,3,1))
    call get_parameter("vx_bnd_zr", qp_bnd(ivx,3,2))
    call get_parameter("vy_bnd_xl", qp_bnd(ivy,1,1))
    call get_parameter("vy_bnd_xr", qp_bnd(ivy,1,2))
    call get_parameter("vy_bnd_yl", qp_bnd(ivy,2,1))
    call get_parameter("vy_bnd_yr", qp_bnd(ivy,2,2))
    call get_parameter("vy_bnd_zl", qp_bnd(ivy,3,1))
    call get_parameter("vy_bnd_zr", qp_bnd(ivy,3,2))
    call get_parameter("vz_bnd_xl", qp_bnd(ivz,1,1))
    call get_parameter("vz_bnd_xr", qp_bnd(ivz,1,2))
    call get_parameter("vz_bnd_yl", qp_bnd(ivz,2,1))
    call get_parameter("vz_bnd_yr", qp_bnd(ivz,2,2))
    call get_parameter("vz_bnd_zl", qp_bnd(ivz,3,1))
    call get_parameter("vz_bnd_zr", qp_bnd(ivz,3,2))
#ifdef ADI
    call get_parameter("pr_bnd_xl", qp_bnd(ipr,1,1))
    call get_parameter("pr_bnd_xr", qp_bnd(ipr,1,2))
    call get_parameter("pr_bnd_yl", qp_bnd(ipr,2,1))
    call get_parameter("pr_bnd_yr", qp_bnd(ipr,2,2))
    call get_parameter("pr_bnd_zl", qp_bnd(ipr,3,1))
    call get_parameter("pr_bnd_zr", qp_bnd(ipr,3,2))
#endif /* ADI */
#ifdef BFIELD
    call get_parameter("bx_bnd_xl", qp_bnd(ibx,1,1))
    call get_parameter("bx_bnd_xr", qp_bnd(ibx,1,2))
    call get_parameter("bx_bnd_yl", qp_bnd(ibx,2,1))
    call get_parameter("bx_bnd_yr", qp_bnd(ibx,2,2))
    call get_parameter("bx_bnd_zl", qp_bnd(ibx,3,1))
    call get_parameter("bx_bnd_zr", qp_bnd(ibx,3,2))
    call get_parameter("by_bnd_xl", qp_bnd(iby,1,1))
    call get_parameter("by_bnd_xr", qp_bnd(iby,1,2))
    call get_parameter("by_bnd_yl", qp_bnd(iby,2,1))
    call get_parameter("by_bnd_yr", qp_bnd(iby,2,2))
    call get_parameter("by_bnd_zl", qp_bnd(iby,3,1))
    call get_parameter("by_bnd_zr", qp_bnd(iby,3,2))
    call get_parameter("bz_bnd_xl", qp_bnd(ibz,1,1))
    call get_parameter("bz_bnd_xr", qp_bnd(ibz,1,2))
    call get_parameter("bz_bnd_yl", qp_bnd(ibz,2,1))
    call get_parameter("bz_bnd_yr", qp_bnd(ibz,2,2))
    call get_parameter("bz_bnd_zl", qp_bnd(ibz,3,1))
    call get_parameter("bz_bnd_zr", qp_bnd(ibz,3,2))
#endif /* BFIELD */

! assign the names with corresponding variables
!
    cvars(idn) = 'dens'
    cvars(imx) = 'momx'
    cvars(imy) = 'momy'
    cvars(imz) = 'momz'
#ifdef ADI
    cvars(ien) = 'ener'
#endif /* ADI */
#ifdef BFIELD
#ifdef GLM
    cvars(iph) = 'bphi'
    cvars(ibx) = 'magx'
    cvars(iby) = 'magy'
    cvars(ibz) = 'magz'
#else /* GLM */
    cvars(iax) = 'potx'
    cvars(iay) = 'poty'
    cvars(iaz) = 'potz'
#endif /* GLM */
#endif /* BFIELD */

    pvars(idn) = 'dens'
    pvars(imx) = 'velx'
    pvars(imy) = 'vely'
    pvars(imz) = 'velz'
#ifdef ADI
    pvars(ien) = 'pres'
#endif /* ADI */
#ifdef BFIELD
    pvars(ibx) = 'magx'
    pvars(iby) = 'magy'
    pvars(ibz) = 'magz'
#ifdef GLM
    pvars(iph) = 'bphi'
#endif /* GLM */
#ifdef RESISTIVITY
    pvars(ijx) = 'curx'
    pvars(ijy) = 'cury'
    pvars(ijz) = 'curz'
#ifdef ANOMALOUS
    pvars(iet) = 'leta'
#endif /* ANOMALOUS */
#endif /* RESISTIVITY */
#endif /* BFIELD */

! reset the shock location indicators
!
#ifdef SHOCK_DETECTION
    sh(:,:,:)  = .false.
#else /* SHOCK_DETECTION */
    sx(:)      = .false.
    sy(:)      = .false.
    sz(:)      = .false.
#endif /* SHOCK_DETECTION */

! assign the conservative variable pointer to the first array
!
    uu => u0

#ifdef PROFILE
! stop accounting time for variable initialization
!
    call stop_timer(ivi)
#endif /* PROFILE */

!-------------------------------------------------------------------------------
!
  end subroutine initialize_variables
!
!===============================================================================
!
! subroutine FINALIZE_VARIABLES:
! -----------------------------
!
!   Subroutine releases memory used by module arrays.
!
!===============================================================================
!
  subroutine finalize_variables()

! local variables are not implicit by default
!
    implicit none
!
!-------------------------------------------------------------------------------
!
#ifdef PROFILE
! start accounting time for variable termination
!
    call start_timer(ivi)
#endif /* PROFILE */

! deallocate variables
!
    if (allocated(u0))     deallocate(u0)
    if (allocated(u1))     deallocate(u1)
    if (allocated(du))     deallocate(du)
    if (allocated(qp))     deallocate(qp)
#ifdef VPOT
    if (allocated(bi))     deallocate(bi)
#endif /* VPOT */
#ifdef SHOCK_DETECTION
    if (allocated(sh))     deallocate(sh)
#endif /* SHOCK_DETECTION */
    if (allocated(sx))     deallocate(sx)
    if (allocated(sy))     deallocate(sy)
    if (allocated(sz))     deallocate(sz)

! deallocate arrays of one dimensional vectors of variables and fluxes
!
    if (allocated(qx))     deallocate(qx)
    if (allocated(qy))     deallocate(qy)
    if (allocated(qz))     deallocate(qz)
    if (allocated(fx))     deallocate(fx)
    if (allocated(fy))     deallocate(fy)
    if (allocated(fz))     deallocate(fz)

! deallocate arrays for the boundary values
!
    if (allocated(qp_bnd)) deallocate(qp_bnd)

! deallocate arrays of variable names
!
    if (allocated(cvars))  deallocate(cvars)
    if (allocated(pvars))  deallocate(pvars)

#ifdef PROFILE
! stop accounting time for variable termination
!
    call stop_timer(ivi)
#endif /* PROFILE */

!-------------------------------------------------------------------------------
!
  end subroutine finalize_variables
!
!===============================================================================
!
! subroutine REDISTRIBUTE_VARIABLES:
! ---------------------------------
!
!   Subroutine initializes array U₁ with values from U₀.
!
!===============================================================================
!
  subroutine redistribute_variables()

! local variables are not implicit by default
!
    implicit none
!
!-------------------------------------------------------------------------------
!
#ifdef PROFILE
! start accounting time for variable redistribution
!
    call start_timer(ivi)
#endif /* PROFILE */

! redistribute variables
!
    if (allocated(u0) .and. allocated(u1)) u1(:,:,:,:) = u0(:,:,:,:)

#ifdef PROFILE
! stop accounting time for redistribution
!
    call stop_timer(ivi)
#endif /* PROFILE */

!-------------------------------------------------------------------------------
!
  end subroutine redistribute_variables

!===============================================================================
!
end module variables
