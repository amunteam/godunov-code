!!******************************************************************************
!!
!!  This file is part of the GODUNOV source code, a program to perform
!!  Newtonian or relativistic magnetohydrodynamical simulations.
!!
!!  Copyright (C) 2007-2020 Grzegorz Kowal <grzegorz@amuncode.org>
!!
!!  This program is free software: you can redistribute it and/or modify
!!  it under the terms of the GNU General Public License as published by
!!  the Free Software Foundation, either version 3 of the License, or
!!  (at your option) any later version.
!!
!!  This program is distributed in the hope that it will be useful,
!!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!!  GNU General Public License for more details.
!!
!!  You should have received a copy of the GNU General Public License
!!  along with this program.  If not, see <http://www.gnu.org/licenses/>.
!!
!!******************************************************************************
!!
!! module: SHAPES
!!
!!  This modules handles the update of specific fixed shapes embedded in the
!!  domain.
!!
!!******************************************************************************
!
module shapes

#ifdef PROFILE
! include external procedures
!
  use timers, only : set_timer, start_timer, stop_timer
#endif /* PROFILE */

! module variables are not implicit by default
!
  implicit none

#ifdef PROFILE
! timer indices
!
  integer, save :: isi, isu
#endif /* PROFILE */

! by default everything is private
!
  private

#ifdef SHAPES
! declare public subroutines
!
  public :: initialize_shapes, update_shapes

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
!
  contains
!
!===============================================================================
!
! subroutine INITIALIZE_SHAPES:
! ----------------------------
!
!   Subroutine initializes module SHAPES.
!
!===============================================================================
!
  subroutine initialize_shapes()

! local variables are not implicit by default
!
    implicit none
!
!-------------------------------------------------------------------------------
!
#ifdef PROFILE
! set timer descriptions
!
    call set_timer('initialize shapes', isi)
    call set_timer('shapes update'    , isu)

! start accounting time for the shape initialization
!
    call start_timer(isi)
#endif /* PROFILE */

#ifdef PROFILE
! stop accounting time for the shape initialization
!
    call stop_timer(isi)
#endif /* PROFILE */

!-------------------------------------------------------------------------------
!
  end subroutine initialize_shapes
!
!===============================================================================
!
! subroutine UPDATE_SHAPES:
! ------------------------
!
!   Subroutine resets dU in the locations belonging to defined shapes.
!
!===============================================================================
!
  subroutine update_shapes()

! include external variables
!
    use variables, only : du

! local variables are not implicit by default
!
    implicit none
!
!-------------------------------------------------------------------------------
!
#ifdef PROFILE
! start accounting time for the shape update
!
    call start_timer(isu)
#endif /* PROFILE */

#ifdef PROFILE
! stop accounting time for the shape update
!
    call stop_timer(isu)
#endif /* PROFILE */

!-------------------------------------------------------------------------------
!
  end subroutine update_shapes
#endif /* SHAPES */

!===============================================================================
!
end module shapes
