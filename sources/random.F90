!!******************************************************************************
!!
!!  This file is part of the GODUNOV source code, a program to perform
!!  Newtonian or relativistic magnetohydrodynamical simulations.
!!
!!  Copyright (C) 2007-2020 Grzegorz Kowal <grzegorz@amuncode.org>
!!
!!  This program is free software: you can redistribute it and/or modify
!!  it under the terms of the GNU General Public License as published by
!!  the Free Software Foundation, either version 3 of the License, or
!!  (at your option) any later version.
!!
!!  This program is distributed in the hope that it will be useful,
!!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!!  GNU General Public License for more details.
!!
!!  You should have received a copy of the GNU General Public License
!!  along with this program.  If not, see <http://www.gnu.org/licenses/>.
!!
!!******************************************************************************
!!
!! module: RANDOM
!!
!!  This module provides subroutines to random number generators.
!!
!!  References:
!!
!!    [1] Marsaglia, G. & Tsang, W.W. (2000) `The ziggurat method for generating
!!        random variables', J. Statist. Software, v5(8)
!!
!!******************************************************************************
!
module random

#ifdef PROFILE
! include external subroutines
!
  use timers, only : set_timer, start_timer, stop_timer
#endif /* PROFILE */

! declare all module variables as implicit
!
  implicit none

#ifdef PROFILE
! timer indices
!
  integer            , save :: iri, irc
#endif /* PROFILE */

! random generator type
!
  character(len = 32), save :: gentype = "same"

! variables to store seeds for random generator
!
  integer                                   , save :: kp = 0
  integer                                   , save :: nseeds, lseed
  integer(kind=4), dimension(:), allocatable, save :: seeds
  integer(kind=4), dimension(128)           , save :: kn
  real   (kind=4), dimension(128)           , save :: fn, wn

! by default everything is private
!
  private

! declare public subroutines
!
  public :: initialize_random, finalize_random
  public :: nseeds, set_seeds, get_seeds, randomu, randomc, randomn

  contains
!
!===============================================================================
!
! subroutine INITIALIZE_RANDOM:
! ----------------------------
!
!   subroutine initializes random number generator;
!
!   Arguments:
!
!     nthreads - the number of total threads;
!     nthread  - the number of current thread;
!
!===============================================================================
!
  subroutine initialize_random(nthreads, nthread)

! obtain required variables from other modules
!
    use parameters, only : get_parameter

! declare all variables as implicit
!
    implicit none

! subroutine arguments
!
    integer, intent(in) :: nthreads, nthread

! local variables
!
    integer      :: i
    real(kind=4) :: r
    real(kind=8) :: dn, tn, q

! parameters
!
    real(kind=8), parameter :: m1 = 2.14748364800000d+09
    real(kind=8), parameter :: vn = 9.91256303526217d-03
!
!-------------------------------------------------------------------------------
!
#ifdef PROFILE
! set timer descriptions
!
    call set_timer('random generator initialization', iri)
    call set_timer('random number generation'       , irc)

! start accounting time for the random number generator
!
    call start_timer(iri)
#endif /* PROFILE */

! set the thread number
!
    kp = nthread

! obtain the generator type
!
    call get_parameter("gentype", gentype)

! calculate the number of seeds
!
    nseeds = nthreads
    lseed  = nseeds - 1

! allocate seeds for random number generator
!
    allocate(seeds(0:lseed))

! prepare the seeds depending on the type of generator
!
    select case(gentype)
    case('random')
      do i = 0, lseed
        call random_number(r)
        seeds(i) = 123456789 * r
      end do
    case default
      r = 0.1234567890123456789
      do i = 0, lseed
        seeds(i) = 123456789 * r
      end do
    end select

! prepare the arrays used by non-uniform distribution generators
!
    dn = 3.442619855899d+00
    tn = 3.442619855899d+00

    q  = vn / exp(-0.5d+00 * dn * dn)

    kn(1)   = int((dn / q) * m1)
    kn(2)   = 0

    wn(1)   = real(q  / m1, kind=4)
    wn(128) = real(dn / m1, kind=4)

    fn(1)   = 1.0e+00
    fn(128) = real(exp(-0.5d+00 * dn * dn), kind=4)

    do i = 127, 2, -1
      dn      = sqrt(-2.0d+00 * log(vn / dn + exp(-0.5d+00 * dn * dn)))
      kn(i+1) = int((dn / tn) * m1)
      tn      = dn
      fn(i)   = real(exp(-0.5d+00 * dn * dn), kind=4)
      wn(i)   = real(dn / m1, kind=4)
    end do

#ifdef PROFILE
! stop accounting time for the random number generator
!
    call stop_timer(iri)
#endif /* PROFILE */

!-------------------------------------------------------------------------------
!
  end subroutine initialize_random
!
!===============================================================================
!
! subroutine FINALIZE_RANDOM:
! --------------------------
!
!   subroutine releases memory allocated by random number generator variables;
!
!===============================================================================
!
  subroutine finalize_random()

! declare all variables as implicit
!
    implicit none
!
!-------------------------------------------------------------------------------
!
#ifdef PROFILE
! start accounting time for the random number generator
!
    call start_timer(iri)
#endif /* PROFILE */

! deallocate seeds if they are allocated
!
    if (allocated(seeds)) deallocate(seeds)

#ifdef PROFILE
! stop accounting time for the random number generator
!
    call stop_timer(iri)
#endif /* PROFILE */

!-------------------------------------------------------------------------------
!
  end subroutine finalize_random
!
!===============================================================================
!
! subroutine SET_SEEDS:
! --------------------
!
!   subroutine sets the seeds from the given array;
!
!===============================================================================
!
  subroutine set_seeds(np, seed)

! declare all variables as implicit
!
    implicit none

! input arguments
!
    integer                           , intent(in) :: np
    integer(kind=4), dimension(0:np-1), intent(in) :: seed
!
!-------------------------------------------------------------------------------
!
#ifdef PROFILE
! start accounting time for the random number generator
!
    call start_timer(iri)
#endif /* PROFILE */

! set the seeds only if the input array and seeds have the same sizes
!
    if (np .eq. nseeds) then

      seeds(0:lseed) = seed(0:lseed)

    end if

#ifdef PROFILE
! stop accounting time for the random number generator
!
    call stop_timer(iri)
#endif /* PROFILE */

!-------------------------------------------------------------------------------
!
  end subroutine set_seeds
!
!===============================================================================
!
! subroutine GET_SEEDS:
! --------------------
!
!   subroutine returns the seeds through an array;
!
!===============================================================================
!
  subroutine get_seeds(seed)

! declare all variables as implicit
!
    implicit none

! output arguments
!
    integer(kind=4), dimension(0:lseed), intent(out) :: seed
!
!-------------------------------------------------------------------------------
!
#ifdef PROFILE
! start accounting time for the random number generator
!
    call start_timer(iri)
#endif /* PROFILE */

    seed(0:lseed) = seeds(0:lseed)

#ifdef PROFILE
! stop accounting time for the random number generator
!
    call stop_timer(iri)
#endif /* PROFILE */

!-------------------------------------------------------------------------------
!
  end subroutine get_seeds
!
!===============================================================================
!
! function SHR3:
! ----------------
!
!   Function evaluates the SHR3 generator and returns a random integer value.
!
!   References:
!
!     [1] Marsaglia, G. and Tsang, W.-W.,
!         "The Ziggurat Method for Generating Random Variables",
!         Journal of Statistical Software, 2000, vol. 5, no. 8, pp. 1-7,
!         http://www.jstatsoft.org/v05/i08
!
!===============================================================================
!
  function shr3() result(ir)

! local variables are not implicit by default
!
    implicit none

! output variables
!
    integer(kind=4) :: ir

! local variables
!
    integer(kind=4) :: ip, it
!
!-------------------------------------------------------------------------------
!
    ip = seeds(kp)
    it = ip

    it = ieor(it, ishft(it,  13))
    it = ieor(it, ishft(it, -17))
    it = ieor(it, ishft(it,   5))

    seeds(kp) = it

    ir = ip + it

    return
!
!-------------------------------------------------------------------------------
!
  end function shr3
!
!===============================================================================
!
! function RANDOMU:
! ----------------
!
!   Function generates uniformly distributed random numbers in range 0.0..amp.
!
!   Arguments:
!
!     amp - the scaling of random number range;
!
!===============================================================================
!
  function randomu(amp) result(val)

! declare all variables as implicit
!
    implicit none

! input/output variables
!
    real, intent(in) :: amp
    real             :: val

! local parameters
!
    real, parameter  :: fs = 2.0**(-32)
!
!-------------------------------------------------------------------------------
!
#ifdef PROFILE
! start accounting time for the random number generation
!
    call start_timer(irc)
#endif /* PROFILE */

    val = amp * (0.5 + fs * shr3())

#ifdef PROFILE
! stop accounting time for the random number generation
!
    call stop_timer(irc)
#endif /* PROFILE */

    return

!-------------------------------------------------------------------------------
!
  end function randomu
!
!===============================================================================
!
! function RANDOMC:
! ----------------
!
!   Function generates uniformly distributed random numbers in range -amp..amp.
!
!   Arguments:
!
!     amp - the scaling of random number range;
!
!===============================================================================
!
  function randomc(amp) result(val)

! declare all variables as implicit
!
    implicit none

! input/output variables
!
    real, intent(in) :: amp
    real             :: val

! local parameters
!
    real, parameter  :: fs = 2.0**(-31)
!
!-------------------------------------------------------------------------------
!
#ifdef PROFILE
! start accounting time for the random number generation
!
    call start_timer(irc)
#endif /* PROFILE */

    val = amp * fs * shr3()

#ifdef PROFILE
! stop accounting time for the random number generation
!
    call stop_timer(irc)
#endif /* PROFILE */

    return

!-------------------------------------------------------------------------------
!
  end function randomc
!
!===============================================================================
!
! function RANDOMN:
! ----------------
!
!   Function generates random numbers with the normal distribution.
!
!   Arguments:
!
!     wth - the distribution width;
!
!===============================================================================
!
  function randomn(wth) result(val)

! declare all variables as implicit
!
    implicit none

! input/output variables
!
    real, intent(in) :: wth
    real             :: val

! local variables
!
    integer(kind=4) :: hz, iz
    real(kind=4)    :: x, y, z, t

! parameters
!
    real(kind=4), parameter :: r = 3.442620e+00
!
!-------------------------------------------------------------------------------
!
#ifdef PROFILE
! start accounting time for the random number generation
!
    call start_timer(irc)
#endif /* PROFILE */

    hz = shr3()
    iz = iand(hz, 127)

    if (abs(hz) < kn(iz+1)) then
      val = real(hz, kind=4) * wn(iz+1)
    else
      do
        if (iz == 0) then
          do
            x = - 0.2904764e+00 * log(randomu(1.0e+00))
            y = - log(randomu(1.0e+00))
            if ((x * x) <= (y + y)) then
              exit
            end if
          end do

          if (hz <= 0) then
            val = - r - x
          else
            val = + r + x
          end if

          exit
        end if

        x = real(hz, kind=4) * wn(iz+1)
        z = fn(iz+1) + randomu(1.0e+00) * (fn(iz) - fn(iz+1))
        t = exp(-0.5e+00 * x * x)

        if (z < t) then
          val = x
          exit
        end if

        hz = shr3()
        iz = iand(hz, 127)

        if (abs(hz) < kn(iz+1)) then
          val = real(hz, kind=4) * wn(iz+1)
          exit
        end if
      end do
    end if

! rescale the return value
!
    val = wth * val

#ifdef PROFILE
! stop accounting time for the random number generation
!
    call stop_timer(irc)
#endif /* PROFILE */

    return

!-------------------------------------------------------------------------------
!
  end function randomn

!===============================================================================
!
end module random
