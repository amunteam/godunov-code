!!******************************************************************************
!!
!!  This file is part of the GODUNOV source code, a program to perform
!!  Newtonian or relativistic magnetohydrodynamical simulations.
!!
!!  Copyright (C) 2007-2020 Grzegorz Kowal <grzegorz@amuncode.org>
!!
!!  This program is free software: you can redistribute it and/or modify
!!  it under the terms of the GNU General Public License as published by
!!  the Free Software Foundation, either version 3 of the License, or
!!  (at your option) any later version.
!!
!!  This program is distributed in the hope that it will be useful,
!!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!!  GNU General Public License for more details.
!!
!!  You should have received a copy of the GNU General Public License
!!  along with this program.  If not, see <http://www.gnu.org/licenses/>.
!!
!!******************************************************************************
!!
!! program: GODUNOV
!!
!!  Godunov is a code to perform numerical simulations in a fluid approximation
!!  on uniform mesh for Newtonian and relativistic environments with or without
!!  magnetic field.
!!
!!******************************************************************************
!
program godunov

! include external subroutines used in this module
!
  use boundaries    , only : initialize_boundaries
  use dataio        , only : initialize_dataio, store_snapshot                 &
                           , read_restart_snapshot, store_restart_snapshot     &
                           , restart_from_snapshot, next_tout
  use equations     , only : initialize_equations
  use evolution     , only : initialize_evolution, finalize_evolution          &
                           , advance, new_time_step
#ifdef FORCING
  use forcing       , only : initialize_forcing, finalize_forcing
#endif /* FORCING */
  use integrals     , only : initialize_integrals, finalize_integrals          &
                           , store_integrals
  use interpolations, only : initialize_interpolations, finalize_interpolations
  use mesh          , only : initialize_mesh, finalize_mesh
  use mesh          , only : step, time, dt, dti, dtn, im, jm, km
  use mpitools      , only : initialize_mpi, finalize_mpi, setup_mpi
#ifdef MPI
  use mpitools      , only : bcast_integer_variable
  use mpitools      , only : reduce_maximum_integer, reduce_sum_real_array
#endif /* MPI */
  use mpitools      , only : master, nprocs, nproc, pdims, pcoords
  use operators     , only : initialize_operators, finalize_operators
  use parameters    , only : read_parameters, finalize_parameters
  use parameters    , only : get_parameter
#ifdef MPI
  use parameters    , only : redistribute_parameters
#endif /* MPI */
  use problems      , only : initialize_problem
  use random        , only : initialize_random, finalize_random
  use schemes       , only : initialize_schemes, finalize_schemes
#ifdef SHAPES
  use shapes        , only : initialize_shapes
#endif /* SHAPES */
  use sources       , only : initialize_sources
  use timers        , only : initialize_timers, start_timer, stop_timer        &
                           , set_timer, get_timer, get_timer_total             &
                           , timer_enabled, timer_description, ntimers
  use user_problem  , only : initialize_user_problem, finalize_user_problem
  use variables     , only : initialize_variables, finalize_variables          &
                           , redistribute_variables

! module variables are not implicit by default
!
  implicit none

! default parameters
!
  integer, dimension(3) :: div    = 1
  logical, dimension(3) :: per    = .true.
  integer               :: nmax   = huge(1), ndat = 1
  real                  :: tmax   = 0.0d+00, trun  = 9.999d+03, tsav  = 3.0d+01
  real                  :: dtnext = 0.0d+00

! flag to adjust time precisely to the snapshots
!
  logical        , save :: precise_snapshots = .false.
  character(len=255)    :: prec_snap         = "off"

! temporary variables
!
  character(len=64)     :: lbnd, ubnd

! the termination and status flags
!
  integer               :: iterm, iret
  character(len=14)     :: info = 'start'

! timer indices
!
  integer               :: iin, iev, iio, itm
#ifdef PROFILE
  integer               :: ipr, ipi
#endif /* PROFILE */

! local snapshot file counters
!
  integer               :: nrun = 1

! iteration and time variables
!
  integer               :: i, ed, eh, em, es, ec
  integer               :: nsteps = 1
  character(len=80)     :: fmt, tmp

  real                  :: tbeg, thrs
  real(kind=8)          :: tm_curr, tm_exec, tm_conv

! an array to store execution times
!
  real(kind=8), dimension(ntimers) :: tm

#ifdef INTEL
! the type of the function SIGNAL should be defined for Intel compiler
!
  integer(kind=4)       :: signal
#endif /* INTEL */

#ifdef SIGNALS
! references to functions handling signals
!
#ifdef GNU
  intrinsic signal
#endif /* GNU */
  external  terminate

! signal definitions
!
  integer, parameter :: SIGINT = 2, SIGABRT = 6, SIGTERM = 15
#endif /* SIGNALS */

! common block
!
  common /termination/ iterm
!
!-------------------------------------------------------------------------------
!
! initialize the termination flag
!
  iterm = 0

! initialize timers
!
  call initialize_timers()

! set timer descriptions
!
  call set_timer('INITIALIZATION'   , iin)
  call set_timer('EVOLUTION'        , iev)
  call set_timer('I/O OPERATIONS'   , iio)
  call set_timer('TERMINATION'      , itm)
#ifdef PROFILE
  call set_timer('parameter reading', ipr)
  call set_timer('problem setup'    , ipi)
#endif /* PROFILE */

! start time accounting for the initialization
!
  call start_timer(iin)

#ifdef SIGNALS
! assign function terminate() with signals
!
#ifdef GNU
  iret = signal(SIGINT , terminate)
  iret = signal(SIGABRT, terminate)
  iret = signal(SIGTERM, terminate)
#else /* GNU */
  iret = signal(SIGINT , terminate, -1)
  iret = signal(SIGABRT, terminate, -1)
  iret = signal(SIGTERM, terminate, -1)
#endif /* GNU */
#endif /* SIGNALS */

! initialize module mpitools
!
  call initialize_mpi()

! print the welcome message
!
  if (master) then

    write (*,'(80("-"))')
    write (*,'(1x,2("="),10x,a22,10x,34("="))') 'The GODUNOV code v2.5'
    write (*,'(1x,2("="),2x,a,2x,34("="))') 'Copyright (C) 2007-2020 ' //      &
                                            'Grzegorz Kowal'
    write (*,'(80("-"))')
    write (*,'(a)') ''

  end if

#ifdef PROFILE
! start time accounting for the parameter reading
!
  call start_timer(ipr)
#endif /* PROFILE */

! initialize and read parameters from the parameter file
!
  if (master) call read_parameters(iterm)

#ifdef MPI
! broadcast the termination flag
!
  call bcast_integer_variable(iterm, iret)

! check if the termination flag was broadcaster successfully
!
  if (iterm .gt. 0) go to 20

! reset the termination flag
!
  iterm = 0

! redistribute parameters among all processors
!
  call redistribute_parameters(iterm)

! reduce the termination flag over all processors to check if everything is fine
!
  call reduce_maximum_integer(iterm, iret)
#endif /* MPI */

#ifdef PROFILE
! stop time accounting for the parameter reading
!
  call stop_timer(ipr)
#endif /* PROFILE */

! check if the parameters were initialized without problems
!
  if (iterm .gt. 0) go to 20

! reset the termination flag
!
  iterm = 0

! obtain the MPI division configuration from the parameter module
!
  call get_parameter("ip" , div(1))
  call get_parameter("jp" , div(2))
#ifdef R3D
  call get_parameter("kp" , div(3))
#endif /* R3D */

! check if the domain is periodic
!
  lbnd = "periodic"
  ubnd = "periodic"
  call get_parameter("xlbndry" , lbnd)
  call get_parameter("xubndry" , ubnd)
  per(1) = (lbnd == "periodic") .and. (ubnd == "periodic")
  lbnd = "periodic"
  ubnd = "periodic"
  call get_parameter("ylbndry" , lbnd)
  call get_parameter("yubndry" , ubnd)
  per(2) = (lbnd == "periodic") .and. (ubnd == "periodic")
#ifdef R3D
  lbnd = "periodic"
  ubnd = "periodic"
  call get_parameter("zlbndry" , lbnd)
  call get_parameter("zubndry" , ubnd)
  per(3) = (lbnd == "periodic") .and. (ubnd == "periodic")
#endif /* R3D */

! get the execution termination parameters
!
  call get_parameter("nmax" , nmax)
  call get_parameter("tmax" , tmax)
  call get_parameter("trun" , trun)
  call get_parameter("tsav" , tsav)

! correct the run time by the save time
!
  trun = trun - tsav / 6.0d+01

! get integral calculation interval
!
  call get_parameter("ndat" , ndat)

! get the precise snapshot flag
!
  call get_parameter("precise_snapshots", prec_snap  )

! set the precise snapshot flag
!
  if (prec_snap == "on"  ) precise_snapshots = .true.
  if (prec_snap == "ON"  ) precise_snapshots = .true.
  if (prec_snap == "true") precise_snapshots = .true.
  if (prec_snap == "TRUE") precise_snapshots = .true.
  if (prec_snap == "yes" ) precise_snapshots = .true.
  if (prec_snap == "YES" ) precise_snapshots = .true.

! set up the MPI geometry
!
  call setup_mpi(div(:), per(:))

! initialize the random number generator
!
  call initialize_random(1, 0)

! initialize mesh
!
  call initialize_mesh(pdims(:), pcoords(:))

! initialize variables
!
  call initialize_variables(im, jm, km)

! initialize boundaries
!
  call initialize_boundaries()

! print informations about used methods
!
  if (master) then

    write (*,'(80("-"))')
    write (*,'(a)') ''

    tmp = 'Physics                   :'
#ifdef HYDRO
    write (tmp, "(a,1x,a)") trim(tmp), 'HD'
#endif /* HYDRO */
#ifdef MHD
    write (tmp, "(a,1x,a)") trim(tmp), 'MHD'
#endif /* MHD */
#ifdef R2D
    write (tmp, "(a,1x,a)") trim(tmp), 'in 2D'
#endif /* R2D */
#ifdef R2D5
    write (tmp, "(a,1x,a)") trim(tmp), 'in 2.5D'
#endif /* R2D5 */
#ifdef R3D
    write (tmp, "(a,1x,a)") trim(tmp), 'in 3D'
#endif /* R3D */
#ifdef ISO
    write (tmp, "(a,1x,a)") trim(tmp), 'with isothermal equation of state'
#endif /* ISO */
#ifdef ADI
    write (tmp, "(a,1x,a)") trim(tmp), 'with adiabatic equation of state'
#endif /* ADI */
    write (*,"(1x,a)") trim(tmp)

  end if

! initialize evolution
!
  call initialize_evolution(master)

! initialize schemes
!
  call initialize_schemes(master)

! initialize interpolations
!
  call initialize_interpolations(master)

  if (master) then

#ifdef BFIELD
    tmp = 'Magnetic field update     :'
#ifdef CT
#ifdef UCT
    write (tmp, "(a,1x,a)") trim(tmp), 'Upwind-CT'
#else /* UCT */
    write (tmp, "(a,1x,a)") trim(tmp), 'CT'
#endif /* UCT */
#endif /* CT */
#ifdef CD
#ifdef FIELDCD
    write (tmp, "(a,1x,a)") trim(tmp), 'Field-CD'
#endif /* FIELDCD */
#ifdef FLUXCD
    write (tmp, "(a,1x,a)") trim(tmp), 'Flux-CD'
#endif /* FLUXCD */
#ifdef AVGCD
    write (tmp, "(a,1x,a)") trim(tmp), 'Averaged-CD'
#endif /* AVGCD */
#endif /* CD */
#ifdef GLM
    write (tmp, "(a,1x,a)") trim(tmp), 'GLM'
#endif /* GLM */
#ifdef VPOT
#endif /* VPOT */
    write (*,"(1x,a)") trim(tmp)
#endif /* BFIELD */

  end if

  if (master) then

#ifdef SHOCK_DETECTION
    tmp = 'Shock detection           :'
    write (tmp, "(a,1x,a)") trim(tmp), 'multidimensional'
    write (*,"(1x,a)") trim(tmp)
#endif /* SHOCK_DETECTION */

  end if

! initialize operators
!
  call initialize_operators(master)

! initialize equations
!
  call initialize_equations(master)

! initialize source terms
!
  call initialize_sources(master)

#ifdef FORCING
! initialize forcing
!
  call initialize_forcing(master)
#endif /* FORCING */

#ifdef SHAPES
! initialize shapes
!
  call initialize_shapes()
#endif /* SHAPES */

! initialize snapshots
!
  call initialize_dataio()

  if (master) then

#ifdef MPI
    tmp = 'Parallelization           :'
    write (tmp, "(a,1x,a)") trim(tmp), 'MPI with'
    write (fmt, "(i9)") nprocs
    write (tmp, "(a,1x,a)") trim(tmp), adjustl(trim(fmt))
    write (tmp, "(a,1x,a)") trim(tmp), 'processor'
    if (nprocs > 1) write (tmp, "(a,a1)") trim(tmp), 's'
    write (*,"(1x,a)") trim(tmp)
#endif /* MPI */

  end if

  if (master) then

#if defined DEBUG || defined PROFILE || defined EXCEPTIONS || defined SIGNALS
    tmp = 'Compiled-in features      :'
#ifdef DEBUG
    write (tmp, "(a,1x,a)") trim(tmp), 'debug'
#endif /* DEBUG */
#ifdef PROFILE
    write (tmp, "(a,1x,a)") trim(tmp), 'profile'
#endif /* PROFILE */
#ifdef EXCEPTIONS
    write (tmp, "(a,1x,a)") trim(tmp), 'exceptions'
#endif /* EXCEPTIONS */
#ifdef SIGNALS
    write (tmp, "(a,1x,a)") trim(tmp), 'signals'
#endif /* SIGNALS */
    write (*,"(1x,a)") trim(tmp)
#endif /* DEBUG | PROFILE | EXCEPTIONS | SIGNALS */

! print precise snapshots flag
!
    write (*,"(1x,a26,':',1x,a)") "Precise snapshot times    ", trim(prec_snap)

    write (*,'(a)') ''
    write (*,'(80("-"))')
    write (*,'(a)') ''

  end if

! initialize dtnext
!
  dtnext = 2.0d+00 * tmax

#ifdef PROFILE
! start time accounting for the problem setup
!
  call start_timer(ipi)
#endif /* PROFILE */

! initialize user problem
!
  call initialize_user_problem(master, iret)

  if (restart_from_snapshot()) then

! wait 1 second before restarting job
!
    call sleep(1)

! start time accounting for I/O operations
!
    call start_timer(iio)

! read initial conditions
!
    call read_restart_snapshot(nrun, iterm)

! stop time accounting for I/O operations
!
    call stop_timer(iio)

  else

! set initial conditions
!
    call initialize_problem(iterm)

! distribute initial conditions to u1 and b1
!
    call redistribute_variables()

! calculate the initial time step
!
    call new_time_step(dtnext)

  end if

! initialize integrals
!
  call initialize_integrals(nrun)

#ifdef PROFILE
! stop time accounting for the problem setup
!
  call stop_timer(ipi)
#endif /* PROFILE */

#ifdef MPI
! reduce termination flag over all processors
!
  call reduce_maximum_integer(iterm, iret)
#endif /* MPI */

! check if the problem was successfully initialized or restarted
!
  if (iterm .gt. 0) go to 10

  if (.not. restart_from_snapshot()) then

! start time accounting for I/O operations
!
    call start_timer(iio)

! store integrals and data cubes
!
    call store_integrals()
    call store_snapshot(nrun, iret)

! inform about stored integrals
!
    if (master .and. iret == 0) info = trim(info) // ' sna'

! stop time accounting for I/O operations
!
    call stop_timer(iio)

#ifdef MPI
! reduce termination flag over all processors
!
    call reduce_maximum_integer(iterm, iret)
#endif /* MPI */

  end if

! if the initial data were not successfully writen, exit the program
!
  if (iterm .gt. 0) go to 10

! reset the termination flag
!
  iterm = 0

! stop time accounting for the initialization
!
  call stop_timer(iin)

! start time accounting for the evolution
!
  call start_timer(iev)

! get current time in seconds
!
  if (master) &
    tbeg = time

! print progress info on master processor
!
  if (master) then

! initialize estimated remaining time of calculations
!
    ed = 9999
    eh =   23
    em =   59
    es =   59

! print progress info
!
    write (*,"('#',a8,a14,2a12,a16,a16)") 'step', 'time', 'dt', 'new dt'       &
                                                  , 'remaining time', 'info'
    write (*,'(a1)') '#'
#ifdef INTEL
    write (*,'(1x,i8,1es14.6,2(1es12.4),2x,1i4.1,"d",1i2.2,"h",1i2.2,"m"' //   &
             ',1i2.2,"s",a16,10x,a1,$)')                                       &
                     step, time, dt, dtn, ed, eh, em, es, trim(info), char(13)
#else /* INTEL */
    write (*,'(1x,i8,1es14.6,2(1es12.4),2x,1i4.1,"d",1i2.2,"h",1i2.2,"m"' //   &
             ',1i2.2,"s",a16,10x,a1)',advance='no')                            &
                     step, time, dt, dtn, ed, eh, em, es, trim(info), char(13)
#endif /* INTEL */

! clear the info string
!
    write(info,"(a14)") ''

  end if

! get current time in seconds
!
  tm_curr = get_timer_total()

! compute elapsed time
!
  thrs = tm_curr / 3.6d+03

! main loop
!
  do while((nsteps <= nmax) .and. (time < tmax) .and. (iterm == 0))

! get the next snapshot time
!
    if (precise_snapshots) dtnext = next_tout() - time

! update variables
!
    call advance(dtnext)

! advance time, the iteration number, and the step in the current run
!
    time   = time   + dt
    step   = step   + 1
    nsteps = nsteps + 1

! check the condition for writing the integrals
!
    if (mod(step, ndat) .eq. 0) then

! start time accounting for I/O operations
!
      call start_timer(iio)

! store the integrals
!
      call store_integrals()

! stop time accounting for I/O operations
!
      call stop_timer(iio)

! inform about stored integrals
!
      if (master) info = trim(info) // ' int'

    end if

! start time accounting for I/O operations
!
    call start_timer(iio)

! store the restart dump
!
    call store_restart_snapshot(thrs, nrun, iret)

! inform about stored integrals
!
    if (master .and. iret == 0) info = trim(info) // ' res'

! store the data snapshots
!
    call store_snapshot(nrun, iret)

! inform about stored integrals
!
    if (master .and. iret == 0) info = trim(info) // ' sna'

! stop time accounting for I/O operations
!
    call stop_timer(iio)

! get current time in seconds
!
    tm_curr = get_timer_total()

! compute elapsed time
!
    thrs = tm_curr / 3.6d+03

! check if the time exceeds execution time limit
!
    if (thrs > trun) iterm = 100

! print info to console
!
    if (master) then

      ec   = int(min(1.0d+00 * huge(ec),                                       &
                             tm_curr * (tmax - time) / max(dti, time - tbeg)))
      es   = max(0, int(mod(ec, 60)))
      em   = int(mod(ec / 60, 60))
      eh   = int(ec / 3600)
      ed   = int(eh / 24)
      eh   = int(mod(eh, 24))
      ed   = min(9999, ed)

#ifdef INTEL
      write (*,'(1x,i8,1es14.6,2(1es12.4),2x,1i4.1,"d",1i2.2,"h",1i2.2,"m"' // &
               ',1i2.2,"s",a16,10x,a1,$)')                                     &
                     step, time, dt, dtn, ed, eh, em, es, trim(info), char(13)
#else /* INTEL */
      write (*,'(1x,i8,1es14.6,2(1es12.4),2x,1i4.1,"d",1i2.2,"h",1i2.2,"m"' // &
               ',1i2.2,"s",a16,10x,a1)',advance='no')                          &
                     step, time, dt, dtn, ed, eh, em, es, trim(info), char(13)
#endif /* INTEL */

! clear the info string
!
      write(info,"(a14)") ''

    end if

! prepare iterm
!
    iterm = max(iterm, iret)

#ifdef MPI
! reduce termination flag over all processors
!
    call reduce_maximum_integer(iterm, iret)
#endif /* MPI */

  end do

! stop time accounting for the evolution
!
  call stop_timer(iev)

! start time accounting for the termination
!
  call start_timer(itm)

! store restart files
!
  if (iterm .ne. 2) then

! start time accounting for I/O operations
!
    call start_timer(iio)

! store the restart dump
!
    call store_restart_snapshot(1.0d+16, nrun, iret)

! inform about stored integrals
!
    if (master .and. iret == 0) info = trim(info) // ' res'

! stop time accounting for I/O operations
!
    call stop_timer(iio)

  end if

! a label to go to if there are any problems, but since all modules have been
! initialized, we have to finalize them first
!
10 continue

! release module variables for all modules
!
  call finalize_user_problem(iret)
  call finalize_integrals()
#ifdef FORCING
  call finalize_forcing()
#endif /* FORCING */
  call finalize_interpolations()
  call finalize_schemes()
  call finalize_evolution()
  call finalize_operators()
  call finalize_variables()
  call finalize_mesh()
  call finalize_random()

! stop time accounting for the termination
!
  call stop_timer(itm)

! get total time
!
  tm(1) = get_timer_total()

! get subtasks timers
!
  do i = 2, ntimers
    tm(i) = get_timer(i)
  end do

#ifdef MPI
! sum up the times from all processors
!
  call reduce_sum_real_array(ntimers, tm(:), iret)
#endif /* MPI */

! print timings only on master processor
!
  if (master) then

! print one empty line
!
    write (*,'(a)') ''

! calculate the conversion factor
!
    tm_conv = 100.0 / tm(1)

! get the execution time
!
    tm_exec = get_timer_total()

! print an empty line
!
    write (*,'(a)') ''

! print the execution times
!
    write (tmp,"(a)") "(2x,a32,1x,':',3x,1f16.3,' secs = ',f6.2,' %')"

    write (*,'(1x,a)') 'EXECUTION TIMINGS'
    do i = 2, ntimers
     if (timer_enabled(i)) write (*,tmp) timer_description(i), tm(i)           &
                                                             , tm_conv * tm(i)
    end do

! print the CPU times
!
    write (tmp,"(a)") "(1x,a14,20x,':',3x,1f16.3,' secs = ',f6.2,' %')"
    write (*,tmp) 'TOTAL CPU TIME', tm(1)         , 100.0
    write (*,tmp) 'TIME PER STEP ', tm(1) / nsteps, 100.0 / nsteps
#ifdef MPI
    write (*,tmp) 'TIME PER CPU  ', tm(1) / nprocs, 100.0 / nprocs
#endif /* MPI */

! convert the execution time to days, hours, minutes, and seconds and print it
!
    tm(1) = tm_exec / 86400.0
    tm(2) = mod(tm_exec / 3600.0, 24.0)
    tm(3) = mod(tm_exec / 60.0, 60.0)
    tm(4) = mod(tm_exec, 60.0)
    tm(5) = nint((tm_exec - floor(tm_exec)) * 1000.0)
    write (tmp,"(a)") "(1x,a14,20x,':',3x,i14,'d'" //                          &
                                       ",i3.2,'h',i3.2,'m',i3.2,'.',i3.3,'s')"
    write (*,tmp) 'EXECUTION TIME', int(tm(1:5))

  end if

! a label to go to if there are any problems
!
20 continue

  if (master) then

! print info about termination due to a signal
!
    if (iterm .ge. 1 .and. iterm .le. 32) then
      write (*,'(a)') ''
      write (*,"(1x,a,i2)") "Terminating program after receiving a" //         &
                                         " termination signal no. ", iterm
      write (*,"(1x,a)") "Restart files have been successfully written."
    end if
    if (iterm .eq. 100) then
      write (*,'(a)') ''
      write (*,"(1x,a)") "Terminating program after exceeding the" //          &
                                                            " execution time."
      write (*,"(1x,a)") "Restart files have been successfully written."
    end if
    if (iterm .ge. 101 .and. iterm .lt. 119) then
      write (*,'(a)') ''
      write (*,"(1x,a)") "The initial conditions for the selected problem" //  &
                         " could not be set."
      write (*,"(1x,a)") "Program has been terminated."
    end if
    if (iterm .ge. 120 .and. iterm .lt. 125) then
      write (*,'(a)') ''
      write (*,"(1x,a)") "Problem with restarting job from restart snapshots."
      write (*,"(1x,a)") "Program has been terminated."
    end if
    if (iterm .ge. 125 .and. iterm .lt. 130) then
      write (*,'(a)') ''
      write (*,"(1x,a)") "Problem with storing snapshots."
      write (*,"(1x,a)") "Program has been terminated."
    end if

  end if

! finalize parameters
!
  call finalize_parameters()

! finalize module mpitools
!
  call finalize_mpi()

!-------------------------------------------------------------------------------
!
end program

#ifdef SIGNALS
!
!===============================================================================
!
! terminate: subroutine sets the iterm variable after receiving a signal
!
!===============================================================================
!
integer(kind=4) function terminate(sig_num)

  implicit none

! input arguments
!
  integer(kind=4), intent(in) :: sig_num
  integer                     :: iterm

! common block
!
  common /termination/ iterm

!-------------------------------------------------------------------------------
!
#ifdef INTEL
  iterm     = sig_num
#else /* INTEL */
  iterm     = 15
#endif /* INTEL */
  terminate = 1

!-------------------------------------------------------------------------------
!
end
#endif /* SIGNALS */

!===============================================================================
!
