;===============================================================================
;
;  function READHDF5_COORDINATE reads coordinates from HDF5 data cubes produced
;  by the Godunov code.
;
;-------------------------------------------------------------------------------
;
;  arguments:
;
;    file        - the file name of the HDF5 data snapshot
;    coordinate  - the coordinate name (xc, yc, zc, xi, yi, or zi)
;    shrink      - the restriction factor, meaning that the data should be
;                  rescaled down (only integer numbers larger than 1)
;    list        - keyword /LIST to list coordinates stored in the file
;    single      - keyword /SINGLE to limit reading from one file only
;                  without merging the whole domain
;
function readhdf5_coordinate, file, coordinate                                 $
                            , shrink = shrink, list = list, single = single

; initiate variables
;
  array = -1
  ncpus = 1

; check it the specified file exists
;
  if (not file_test(file, /regular, /read)) then begin

    print, file + ' does not exist!'

    return, array

  endif

; check it the file is really written in the HDF5 format
;
  if (not h5f_is_hdf5(file)) then begin

    print, file + ' is not a HDF5 file!'

    return, array

  endif

; check if arguments are correct
;
  if (not keyword_set(shrink)) then shrink = 1

; open the file
;
  fid = h5f_open(file)

; check if there were problems with opening the SD interface
;
  if (fid le 0) then begin

    print, 'Could not open ' + file

; close the file
;
    h5f_close, fid

; return -1
;
    return, -1

  endif

; everything goes fine so far, so proceed
;
; if keyword /LIST is defined list all coordinates stored in the file
;
  if (keyword_set(list)) then begin

; open the group of coordinates
;
    gid = h5g_open(fid, 'coordinates')

; get the number of datasets
;
    ndatasets = h5g_get_num_objs(gid)

; print all dataset names
;
    print, 'Number of coordinates stored:', ndatasets
    names = strarr(ndatasets)
    for n = 0, ndatasets - 1 do begin
      nm = h5g_get_obj_name_by_idx(gid, n)
      names[n] = nm
    endfor
    print, 'Coordinate names            :', names

; close the group of coordinates
;
    h5g_close, gid

; close the file
;
    h5f_close, fid

; return 0
;
    return, 0

  endif

; open the group of attributes
;
  gid = h5g_open(fid, 'attributes')

; read attributes
;
  nprocs = 1

; get the number of attributes
;
  nattributes = h5a_get_num_attrs(gid)

; read attributes
;
  for n = 0, nattributes - 1 do begin
    aid = h5a_open_idx(gid, n)
    name = h5a_get_name(aid)
    data = h5a_read(aid)
    case name of
      'nprocs' : nprocs = data[0]
    else :
    endcase
    h5a_close, aid
  endfor

; close the group
;
  h5g_close, gid

; proceed if ncpus == 1 or the keyword /SINGLE is set
;
  if (nprocs eq 1 or keyword_set(single)) then begin

; open the group of coordinates
;
    gid = h5g_open(fid, 'coordinates')

; read coordinates
;
    did   = h5d_open(gid, coordinate)
    array = h5d_read(did)
    h5d_close, did

; close the group
;
    h5g_close, gid

; close the file
;
    h5f_close, fid

  endif else begin ; data in multiple files

; obtain information about the domain dimensions and division
;
    pdims = [1, 1, 1]
    dims  = [1, 1, 1]
    ng    = 0
    nproc = 0

; open the group of attributes
;
    gid = h5g_open(fid, 'attributes')

; read attributes
;
    for n = 0, nattributes - 1 do begin
      aid = h5a_open_idx(gid, n)
      name = h5a_get_name(aid)
      data = h5a_read(aid)
      case name of
        'pdims'  : pdims  = data
        'dims'   : dims   = data
        'ng'     : ng     = data[0]
        'nproc'  : nproc  = data[0]
      else :
      endcase
      h5a_close, aid
    endfor

; close the group
;
    h5g_close, gid

; calculate the size of domain
;
    ng = replicate(ng, n_elements(dims))
    ii = where(dims eq 1, c)
    if (c ge 1) then ng[ii] = 0
    sz = dims - 2 * ng
    rz = sz / shrink

; create coordinate arrays
;
      case coordinate of
        'xc': array = fltarr(pdims[0] * rz[0])
        'yc': array = fltarr(pdims[1] * rz[1])
        'zc': array = fltarr(pdims[2] * rz[2])
        'xi': array = fltarr(pdims[0] * rz[0])
        'yi': array = fltarr(pdims[1] * rz[1])
        'zi': array = fltarr(pdims[2] * rz[2])
      endcase

; close the file
;
    h5f_close, fid

; iterate over all files
;
    pos = strpos(file, string(nproc, format = '(i5.5,".h5")'))

    for p = 0, nprocs-1 do begin

; prepare the filename
;
      strput, file, string(p, format = '(i5.5)'), pos

; open the file
;
      fid = h5f_open(file)

; open the group of attributes
;
      gid = h5g_open(fid, 'attributes')

      for n = 0, nattributes - 1 do begin
        aid = h5a_open_idx(gid, n)
        name = h5a_get_name(aid)
        data = h5a_read(aid)
        case name of
          'pcoords': pcoords  = data
        else :
        endcase
        h5a_close, aid
      endfor

; close the group
;
      h5g_close, gid

; open the group of coordinates
;
      gid = h5g_open(fid, 'coordinates')

; read coordinates
;
      did = h5d_open(gid, coordinate)
      tmp = h5d_read(did)
      h5d_close, did

; close the group
;
      h5g_close, gid

; close the file
;
      h5f_close, fid

; insert coordinate stencil into the full coordinate vector
;
      case coordinate of
        'xc': begin
                ib  = ng[0]
                ie  = ib + sz[0] - 1

                tmp = temporary(tmp[ib:ie])

                ib  = pcoords[0] * rz[0]
                ie  = ib + rz[0] - 1

                array[ib:ie] = 0.0
                for i = 0, shrink - 1 do begin
                  array[ib:ie] = array[ib:ie] + tmp[i:*:shrink] / shrink
                endfor
              end
        'yc': begin
                jb  = ng[1]
                je  = jb + sz[1] - 1

                tmp = temporary(tmp[jb:je])

                jb  = pcoords[1] * rz[1]
                je  = jb + rz[1] - 1

                array[jb:je] = 0.0
                for j = 0, shrink - 1 do begin
                  array[jb:je] = array[jb:je] + tmp[j:*:shrink] / shrink
                endfor
              end
        'zc': begin
                kb  = ng[2]
                ke  = kb + sz[2] - 1

                tmp = temporary(tmp[kb:ke])

                kb  = pcoords[2] * rz[2]
                ke  = kb + rz[2] - 1

                array[kb:ke] = 0.0
                for k = 0, shrink - 1 do begin
                  array[kb:ke] = array[kb:ke] + tmp[k:*:shrink] / shrink
                endfor
              end
        'xi': begin
                ib  = ng[0]
                ie  = ib + sz[0] - 1

                tmp = temporary(tmp[ib:ie])

                ib  = pcoords[0] * rz[0]
                ie  = ib + rz[0] - 1

                array[ib:ie] = tmp[shrink-1:*:shrink]
              end
        'yi': begin
                jb  = ng[1]
                je  = jb + sz[1] - 1

                tmp = temporary(tmp[jb:je])

                jb  = pcoords[1] * rz[1]
                je  = jb + rz[1] - 1

                array[jb:je] = tmp[shrink-1:*:shrink]
              end
        'zi': begin
                kb  = ng[2]
                ke  = kb + sz[2] - 1

                tmp = temporary(tmp[kb:ke])

                kb  = pcoords[2] * rz[2]
                ke  = kb + rz[2] - 1

                array[kb:ke] = tmp[shrink-1:*:shrink]
              end
      endcase

    endfor

  endelse

; return the coordinate values
;
  return, array

end
