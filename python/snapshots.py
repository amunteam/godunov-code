# import classes
#
from tables  import *
from numpy   import zeros
from os.path import basename
from rebin   import rebin

def readhdf5_variable(filename, variable, division):
    '''
	readhdf5_variable: reads a given variable from set of HDF5 files.
    '''
    # Open the given file and read basic attributes needed to reconstruct the
    # domain dimensions.
    #
    h5 = openFile(filename, mode = "r")
    nf = 1
    pm = [1, 1, 1]
    if (h5.root.attributes._v_attrs.__contains__("nprocs")):
	nf = h5.root.attributes._v_attrs.nprocs[0]
    if (h5.root.attributes._v_attrs.__contains__("pdims")):
	pm = h5.root.attributes._v_attrs.pdims
    if (h5.root.attributes._v_attrs.__contains__("ng")):
	ng = h5.root.attributes._v_attrs.ng[0]
    cm = h5.root.attributes._v_attrs.dims
    h5.close()

    # Check if the file is a restart file.
    #
    if (basename(filename)[0] == 'r'):
      restart = True
      cm[0] = max(1, cm[0] - 2 * ng)
      cm[1] = max(1, cm[1] - 2 * ng)
      cm[2] = max(1, cm[2] - 2 * ng)
    else:
      restart = False

    # Prepare the domain dimensions and create an array to store variables.
    #
    rm = cm / division
    dm = pm * rm
    if (cm[2] == 1):
	rm[2] = 1
	dm[2] = 1
    qt = zeros(dm[::-1])

    # Iterate over all files and read given variable.
    #
    for np in range (0, nf):

	# Generate the local file name.
	#
	fl = filename.replace("_00000.", '_%05d.' % np)

	# Open the current file.
	#
	h5 = openFile(fl, mode = "r")

	# Read the cube coordinates in order to know where to insert the
	# current subvolume.
	#
	pc = [0, 0, 0]
	if (h5.root.attributes._v_attrs.__contains__("pdims")):
	    pc = h5.root.attributes._v_attrs.pcoords

	# Read the given variable.
	#
	qs = h5.getNode("/variables", variable).read()

	# Prepare indices of the subvolume.
	#
	ib = 0
	jb = 0
	kb = 0
	if (restart == True):
	    ib = ib + ng
	    jb = jb + ng
	    if (cm[2] > 1):
	      kb = kb + ng
	ie = ib + cm[0]
	je = jb + cm[1]
	ke = kb + cm[2]

	# Resize the subvolume in order to reduce memory usage.
	#
	qr = rebin(qs[kb:ke,jb:je,ib:ie], rm[2], rm[1], rm[0])

	# Prepare indices for the insertion bounds.
	#
	ib = pc[0] * rm[0]
	jb = pc[1] * rm[1]
	kb = pc[2] * rm[2]
	ie = ib + rm[0]
	je = jb + rm[1]
	ke = kb + rm[2]

	# Finally, insert the subvolume to the output array.
	#
	qt[kb:ke,jb:je,ib:ie] = qr

	# Close the HDF5 file.
	#
	h5.close()

    # Return the output array.
    #
    return qt


def readhdf5_coordinate(filename, variable, division):
    '''
	readhdf5_coordinate: read coordinates from an HDF5 file.
    '''
    # Open the given file and read basic attributes needed to reconstruct the
    # coordinates.
    #
    h5 = openFile(filename, mode = "r")
    pm = h5.root.attributes._v_attrs.pdims
    cm = h5.root.attributes._v_attrs.dims
    xm = h5.root.attributes._v_attrs.xmin[0]
    ym = h5.root.attributes._v_attrs.ymin[0]
    zm = h5.root.attributes._v_attrs.zmin[0]
    dx = h5.root.attributes._v_attrs.dx[0] * division
    dy = h5.root.attributes._v_attrs.dy[0] * division
    dz = h5.root.attributes._v_attrs.dz[0] * division
    h5.close()

    # Prepare dimensions.
    #
    rm = cm / division
    dm =  pm * rm

    # Prepare output vectors for each coordinate and return it.
    #
    if (variable == "x" or variable == "xc"):
	x = zeros(dm[0])
	for i in range(0, dm[0]):
	    x[i] = xm + (i + 0.5) * dx
	return x
    if (variable == "y" or variable == "yc"):
	y = zeros(dm[1])
	for j in range(0, dm[1]):
	    y[j] = ym + (j + 0.5) * dy
	return y
    if (variable == "z" or variable == "zc"):
	z = zeros(dm[2])
	for k in range(0, dm[2]):
	    z[k] = zm + (k + 0.5) * dz
	return z
    if (variable == "xi"):
	x = zeros(dm[0])
	for i in range(0, dm[0]):
	    x[i] = xm + (i + 1) * dx
	return x
    if (variable == "yi"):
	y = zeros(dm[1])
	for j in range(0, dm[1]):
	    y[j] = ym + (j + 1) * dy
	return y
    if (variable == "zi"):
	z = zeros(dm[2])
	for k in range(0, dm[2]):
	    z[k] = zm + (k + 1) * dz
	return z


def readhdf5_attribute(filename, attribute):
    '''
	readhdf5_attribute: read attributes from an HDF5 file.
    '''
    # Open the given file and read basic attributes needed to reconstruct the
    # coordinates.
    #
    h5 = openFile(filename, mode = "r")
    if (attribute == "xmin"):
	attr = h5.root.attributes._v_attrs.xmin[0]
    if (attribute == "xmax"):
	attr = h5.root.attributes._v_attrs.xmax[0]
    if (attribute == "ymin"):
	attr = h5.root.attributes._v_attrs.ymin[0]
    if (attribute == "ymax"):
	attr = h5.root.attributes._v_attrs.ymax[0]
    if (attribute == "zmin"):
	attr = h5.root.attributes._v_attrs.zmin[0]
    if (attribute == "zmax"):
	attr = h5.root.attributes._v_attrs.zmax[0]
    if (attribute == "dx"):
	attr = h5.root.attributes._v_attrs.dx[0]
    if (attribute == "dy"):
	attr = h5.root.attributes._v_attrs.dy[0]
    if (attribute == "dz"):
	attr = h5.root.attributes._v_attrs.dz[0]
    if (attribute == "time"):
	attr = h5.root.attributes._v_attrs.time[0]
    if (attribute == "dt"):
	attr = h5.root.attributes._v_attrs.dt[0]
    if (attribute == "step"):
	attr = h5.root.attributes._v_attrs.step[0]
    h5.close()

    # Return the attribute value.
    #
    return attr
